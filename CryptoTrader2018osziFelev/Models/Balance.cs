﻿using CryptoTrader2018osziFelev.Models.Interfaces;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CryptoTrader2018osziFelev.Models
{
    public class Balance : IBalance
    {
        string token;
        double eth;
        double usd;
        double btc;
        double xrp;
        //Dictionary<string, double> balances;

        public double Eth { get => eth; set => eth = value; }
        public double Usd { get => usd; set => usd = value; }
        public double Btc { get => btc; set => btc = value; }
        public double Xrp { get => xrp; set => xrp = value; }
        public string Token { get => token; set => token = value; }

        [JsonConstructor]
        public Balance()
        {

        }

        public Balance(string jsonOrXmlInputText)
        {
            if (InputHelper.Identify(jsonOrXmlInputText) == InputTypes.Json)
            {
                Balance toCopy = JsonConvert.DeserializeObject<Balance>(jsonOrXmlInputText);
                this.Token = toCopy.Token;
                this.btc = toCopy.btc;
                this.eth = toCopy.eth;
                this.usd = toCopy.usd;
                this.xrp = toCopy.xrp;
            }
        }



        public double CalculateTotalValueOfBalancesInUsd(List<IExchangeRate> exchangeRates)
        {
            //symbol+currentrate
            double tots = 0;
            tots += usd;
            exchangeRates = exchangeRates.OrderByDescending(x => x.LastRefreshed).ToList();
            tots += exchangeRates.Where(x => x.Symbol.ToUpper() == "BTC").Select(y => y.CurrentRate * btc).First();
            tots += exchangeRates.Where(x => x.Symbol.ToUpper() == "ETH").Select(y => y.CurrentRate * eth).First();
            tots += exchangeRates.Where(x => x.Symbol.ToUpper() == "XRP").Select(y => y.CurrentRate * xrp).First();
            return tots;
        }
    }
}
