﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Xml;

namespace CryptoTrader2018osziFelev.Models
{
    public enum InputTypes { Json, Xml, None }
    public static class InputHelper
    {
        public static InputTypes Identify(string input)
        {

            try
            {
                JsonConvert.DeserializeObject(input);
                return InputTypes.Json;
            }
            catch (FormatException ex)
            {
                try
                {
                    new XmlDocument().Load(input);
                    return InputTypes.Xml;
                }
                catch (FormatException fx)
                {
                    return InputTypes.None;
                }
            }

        }

        //public static string FormatToCorrectedJson(string jsonInput)
        //{
        //    //hack part 2
        //    //  kiszedi a tokent a balance-ból és az elejére rakja h megegye a deserialize, akárhány elemre.

        //    string modifiedInput = jsonInput;
        //    List<string> splitted = jsonInput.Split("\r\n").ToList();
        //    List<string> tokens = (splitted.Where(x => x.TrimStart('\"', '\'', ' ').StartsWith("token")).ToList());
        //    splitted.RemoveAll(x => x.Contains("token"));
        //    int j = 0;
        //    string jsoned = "";
        //    for (int i = 0; i < splitted.Count; i++)
        //    {
        //        if (j < tokens.Count && splitted[i].Contains("balance"))
        //        {
        //            jsoned += tokens[j];
        //            j++;
        //        }
        //        jsoned += splitted[i];
        //    }
        //    return jsoned;
        //}

    }
}
