﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CryptoTrader2018osziFelev.Models.Interfaces
{
   public interface IHistory
    {
        /*
         SAMPLE STARTS 
          
          [
    {
        "symbol": "BTC",
        "amount": 0.1,
        "type": "Purchase",
        "createdAt": "2018-09-28T13:44:53.7907944+00:00",
        "balance": {
            "token": "19aaa2d2-9299-4a2b-bb3d-06ffc006e4f5",
            "usd": 4333.264637314,
            "btc": 0.1,
            "eth": 0,
            "xrp": 0
        },
        "exchangeRates": {
            "btc": 6667.35362686,
            "eth": 225.31455189,
            "xrp": 0.52732946
        }
    },
    {
        "symbol": "BTC",
        "amount": 0.1,
        "type": "Purchase",
        "createdAt": "2018-09-28T13:56:09.7307327+00:00",
        "balance": {
            "token": "19aaa2d2-9299-4a2b-bb3d-06ffc006e4f5",
            "usd": 3668.3429065519995,
            "btc": 0.2,
            "eth": 0,
            "xrp": 0
        },
        "exchangeRates": {
            "btc": 6649.21730762,
            "eth": 226.01705464,
            "xrp": 0.53143514
        }
    }
]        
     SAMPLE OVER
         * */

        List<IOneHistory> Histories { get; set; }
        //void Initialize(string jsonOrXmlInputText);

    }
}
