﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CryptoTrader2018osziFelev.Models.Interfaces
{
    interface IBalanceWithExchangeRate
    {
        //sry itt nem tudtam mi kéne
        IBalance Balance { get; set; } 
        List<IExchangeRate> ExchangeRates { get; set; } //ctor példányosítani
    }
}
