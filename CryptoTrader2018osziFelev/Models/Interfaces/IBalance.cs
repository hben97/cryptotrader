﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CryptoTrader2018osziFelev.Models.Interfaces
{
   public interface IBalance
    {
        /*
         * SAMPLE DATA
         
        {
    "token": "19aaa2d2-9299-4a2b-bb3d-06ffc006e4f5",
    "usd": 5000,
    "btc": 0,
    "eth": 0,
    "xrp": 0
        }
          
         SAMPLE DATA OVER
         * */


        string Token { get; set; }
        double Usd { get; }
        double Btc { get; }
        double Eth { get; }
        double Xrp { get; }
        

        double CalculateTotalValueOfBalancesInUsd(List<IExchangeRate> exchangeRates);
        
        // Dictionary<string, double> Balances { get; set; } ezt azért vettem ki mert így 1 sor a Parse ha meg dictionarybe akarom, akkor meg bonyi
        //void Initialize(string jsonOrXmlInputText);
        //void Initialize(string tokenInputText, Dictionary<string, double> balancesInputDictionary);
    }
}
