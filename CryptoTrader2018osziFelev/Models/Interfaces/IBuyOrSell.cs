﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CryptoTrader2018osziFelev.Models.Interfaces
{
    public interface IBuyOrSell
    {
        /*
         * SAMPLE DATA
         {
            "Symbol": "BTC",
            "Amount": 0.10
         }      
         
         * SAMPLE DATA OVER */

        //ctor desc.:
        //void Initialize(string buyOrSellUpperCase, string symbol, double amount);
        TransactionType BuyOrSellUpperCase { get; }
        string Symbol { get; }
        string XmlOutputText { get; }
        string JsonOutputText { get; }
        //ctor

    }

}
