﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CryptoTrader2018osziFelev.Models.Interfaces
{
   public interface IExchangeRate
    {
        /*
       SAMPLE INPUT:   
         
         {
    "symbol": "BTC",
    "currentRate": 6605.343108,
    "lastRefreshed": "2018-09-28 13:10:00",
    "timeZone": "UTC",
    "history": {
        "2018-09-28 13:10:00": 6605.343108,
        "2018-09-28 13:05:00": 6589.75695983,
        "2018-09-28 13:00:00": 6617.60552599,
        "2018-09-28 12:55:00": 6614.842052,
        "2018-09-28 12:50:00": 6615.14330371,
        "2018-09-28 12:45:00": 6630.37493455,
        "2018-09-28 12:40:00": 6632.3604782,
        "2018-09-28 12:35:00": 6626.81649079
                }
        }

            SAMPLE INPUT OVER
         */

       // void Initialize(string jsonOrXmlInputText);
        string Symbol { get; }
        double CurrentRate { get; }
        DateTime LastRefreshed { get; }
        string TimeZone { get; }
        Dictionary<DateTime, double> History { get; }
    }
}
