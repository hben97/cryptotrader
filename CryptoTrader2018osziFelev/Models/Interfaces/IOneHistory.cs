﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CryptoTrader2018osziFelev.Models.Interfaces
{
    public interface IOneHistory
    {
        /*
         {
        "symbol": "BTC",
        "amount": 0.1,
        "type": "Purchase",
        "createdAt": "2018-09-28T13:56:09.7307327+00:00",
        "balance": {
            "token": "19aaa2d2-9299-4a2b-bb3d-06ffc006e4f5",
            "usd": 3668.3429065519995,
            "btc": 0.2,
            "eth": 0,
            "xrp": 0
        },
        "exchangeRates": {
            "btc": 6649.21730762,
            "eth": 226.01705464,
            "xrp": 0.53143514
        }
    }
             */


        string Symbol { get; }
        double Amount { get; }
        string Type { get; }
        DateTime CreatedAt { get; }
        //string Token { get; }
        Balance Balance { get; }
        Dictionary<string, double> ExchangeRates { get; }

    }
}
