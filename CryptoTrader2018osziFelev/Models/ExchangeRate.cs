﻿using CryptoTrader2018osziFelev.Models.Interfaces;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CryptoTrader2018osziFelev.Models
{
    public class ExchangeRate : IExchangeRate
    {
        string symbol;
        double currentRate;
        DateTime lastRefreshed;
        string timeZone;
        Dictionary<DateTime, double> history;

        public string Symbol { get => symbol; set => symbol = value; }
        public double CurrentRate { get => currentRate; set => currentRate = value; }
        public DateTime LastRefreshed { get => lastRefreshed; set => lastRefreshed = value; }
        public string TimeZone { get => timeZone; set => timeZone = value; }
        public Dictionary<DateTime, double> History { get => history; set => history = value; }

        [JsonConstructor]
        public ExchangeRate()
        {

        }
        public ExchangeRate(string input)
        {
            if (InputHelper.Identify(input) == InputTypes.Json)
            {
                ExchangeRate toCopy = JsonConvert.DeserializeObject<ExchangeRate>(input);
                this.currentRate = toCopy.currentRate;
                this.history = toCopy.history;
                this.lastRefreshed = toCopy.lastRefreshed;
                this.symbol = toCopy.symbol;
                this.timeZone = toCopy.timeZone;

            }
            else {
                throw new FormatException("bad");
            }
        }
    }
}
