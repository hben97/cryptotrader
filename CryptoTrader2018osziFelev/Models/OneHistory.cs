﻿using CryptoTrader2018osziFelev.Models.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace CryptoTrader2018osziFelev.Models
{
            /*
         {
        "symbol": "BTC",
        "amount": 0.1,
        "type": "Purchase",
        "createdAt": "2018-09-28T13:56:09.7307327+00:00",
      "balance": {                                                -ez egy balance objektum
          "token": "19aaa2d2-9299-4a2b-bb3d-06ffc006e4f5",        -ez egy balance objektum
          "usd": 3668.3429065519995,                              -ez egy balance objektum
          "btc": 0.2,                                             -ez egy balance objektum
          "eth": 0,                                               -ez egy balance objektum
          "xrp": 0                                                -ez egy balance objektum
        },                                                          -ez egy balance objektum
        "exchangeRates": {            
            "btc": 6649.21730762,     
            "eth": 226.01705464,      
            "xrp": 0.53143514         
        }
    }
             */

    public class OneHistory : IOneHistory
    {
        string symbol;
        double amount;
        string type;
        DateTime createdAt;
        string token;
        Balance balance;
        Dictionary<string,double> exchangeRates;

        public double Amount { get => amount; set => amount = value; }
        public string Type { get => type; set => type = value; }
        public DateTime CreatedAt { get => createdAt; set => createdAt = value; }
        public string Token { get => token; set => token = value; }
        public Balance Balance { get => balance; set => balance = value; }
        public Dictionary<string, double> ExchangeRates { get => exchangeRates; set => exchangeRates = value; }
        public string Symbol { get => symbol; set => symbol = value; }


        //deserialize során ő hívódjon meg
        [JsonConstructor]
        public OneHistory()
        {

        }


        public OneHistory(string jsonOrXmlInput)
        {
            if (jsonOrXmlInput != null)
            {
                InputTypes thisType = InputHelper.Identify(jsonOrXmlInput);
                if (thisType == InputTypes.Json)
                {

                  //  string correctJson = InputHelper.FormatToCorrectedJson(jsonOrXmlInput);

                    OneHistory toCopy = JsonConvert.DeserializeObject<OneHistory>(jsonOrXmlInput);

                    this.Symbol = toCopy.Symbol;
                    this.amount = toCopy.Amount;
                    this.token = toCopy.Token;
                    this.balance = toCopy.balance;
                    this.createdAt = toCopy.CreatedAt;
                    this.exchangeRates = toCopy.exchangeRates;
                }
                else if (thisType == InputTypes.Xml)
                {

                }
                else
                {
                    //bad
                    throw new FormatException("API response was baad :(");
                }
            }
        }

        //private string FormatToCorrectJson(string jsonInput)
        //{
        //    //  kiszedi a tokent a balance-ból és az elejére rakja h megegye a deserialize
        //    string modifiedInput = jsonInput;
        //    if (jsonInput.Contains("token"))
        //    {
        //        int startIdx = jsonInput.IndexOf("token") - 1;
        //        int endIdx = startIdx;
        //        while (endIdx < jsonInput.Length && jsonInput[endIdx] != ',')
        //        {
        //            endIdx++;
        //        }
        //        modifiedInput = jsonInput.Remove(startIdx, (endIdx - startIdx) + 1);
        //        modifiedInput = modifiedInput.Insert(1, jsonInput.Substring(startIdx, (endIdx - startIdx) + 1));
        //    }
        //    return modifiedInput;
        //}

        //public string JsonOutput()
        //{
        //    return JsonConvert.SerializeObject(this);
        //}

    }
}
