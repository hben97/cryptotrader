﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CryptoTrader2018osziFelev.Models.Interfaces
{
    public enum TransactionType { BUY, SELL };

    public class BuyOrSell : IBuyOrSell
    {
        [JsonIgnore]
        TransactionType buyOrSellUpperCase;

        string symbol;
        double amount;

        [JsonIgnore]
        public TransactionType BuyOrSellUpperCase { get => buyOrSellUpperCase; set => buyOrSellUpperCase = value; }
        public string Symbol { get => symbol; set => symbol = value; }
        public double Amount { get => amount; set => amount = value; }

        [JsonIgnore]
        public string XmlOutputText => throw new NotImplementedException();

        [JsonIgnore]
        public string JsonOutputText { get => JsonConvert.SerializeObject(this, Formatting.Indented); } //TODO niért hívódikmeg??

        public BuyOrSell(TransactionType tr, string symbol, double amount)
        {
            this.buyOrSellUpperCase = tr;
            this.symbol = symbol;
            this.amount = amount;
        }
    }
}
