﻿using CryptoTrader2018osziFelev.Models.Interfaces;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CryptoTrader2018osziFelev.Models
{
    public class History : IHistory
    {
        List<IOneHistory> histories;

        public List<IOneHistory> Histories { get => histories; set => histories = value; }


        [JsonConstructor]
        public History()
        {

        }
        public History(string jsonOrXmlInput)
        {
            InputTypes thisType = InputHelper.Identify(jsonOrXmlInput);
            if (thisType == InputTypes.Json)
            {
                List<OneHistory> myHistory = JsonConvert.DeserializeObject<List<OneHistory>>(jsonOrXmlInput);

                histories = new List<IOneHistory>();
                foreach (var hist in myHistory)
                {
                    histories.Add(hist);
                }

            }
            else {

                throw new FormatException();
            }
        }
    
  

    }
}
