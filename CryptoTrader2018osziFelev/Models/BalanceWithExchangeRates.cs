﻿using CryptoTrader2018osziFelev.Models.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CryptoTrader2018osziFelev.Models
{
    public class BalanceWithExchangeRates : IBalanceWithExchangeRate
    {
        IBalance balance;
        List<IExchangeRate> exchangeRates;



        public IBalance Balance { get => balance; set => balance = value; }
        public List<IExchangeRate> ExchangeRates { get => exchangeRates; set => exchangeRates = value; }
        
        public BalanceWithExchangeRates()
        {
            exchangeRates = new List<IExchangeRate>();
        }
    }
}
