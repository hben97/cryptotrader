﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using CryptoTrader2018osziFelev.Models;
using CryptoTrader2018osziFelev.Models.Interfaces;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace CryptoTrader2018osziFelev
{
    public class Program
    {
        public static void Main(string[] args)
        {
//            string s = @"[         {
//    'symbol': 'BTC',
//    'currentRate': 6605.343108,
//    'lastRefreshed': '2018-09-28 13:10:00',
//    'timeZone': 'UTC',
//    'history': {
//        '2018-09-28 13:10:00': 6605.343108,
//        '2018-09-28 13:05:00': 6589.75695983,
//        '2018-09-28 13:00:00': 6617.60552599,
//        '2018-09-28 12:55:00': 6614.842052,
//        '2018-09-28 12:50:00': 6615.14330371,
//        '2018-09-28 12:45:00': 6630.37493455,
//        '2018-09-28 12:40:00': 6632.3604782,
//        '2018-09-28 12:35:00': 6626.81649079
//                }
//        },
//  {
//    'symbol': 'ETH',
//    'currentRate': 301.348,
//    'lastRefreshed': '2018-09-28 13:10:00',
//    'timeZone': 'UTC',
//    'history': {
//        '2018-09-28 13:10:00': 6605.343108,
//        '2018-09-28 13:05:00': 6589.75695983,
//        '2018-09-28 13:00:00': 6617.60552599,
//        '2018-09-28 12:55:00': 6614.842052,
//        '2018-09-28 12:50:00': 6615.14330371,
//        '2018-09-28 12:45:00': 6630.37493455,
//        '2018-09-28 12:40:00': 6632.3604782,
//        '2018-09-28 12:35:00': 6626.81649079
//                }
//        },  {
//    'symbol': 'XRP',
//    'currentRate': 14.58,
//    'lastRefreshed': '2018-09-28 13:10:00',
//    'timeZone': 'UTC',
//    'history': {
//        '2018-09-28 13:10:00': 6605.343108,
//        '2018-09-28 13:05:00': 6589.75695983,
//        '2018-09-28 13:00:00': 6617.60552599,
//        '2018-09-28 12:55:00': 6614.842052,
//        '2018-09-28 12:50:00': 6615.14330371,
//        '2018-09-28 12:45:00': 6630.37493455,
//        '2018-09-28 12:40:00': 6632.3604782,
//        '2018-09-28 12:35:00': 6626.81649079
//                }
//        }
//]";
//            List<ExchangeRate> ex = JsonConvert.DeserializeObject<List<ExchangeRate>>(s);
//            List<IExchangeRate> ier = new List<IExchangeRate>();
//            ier.AddRange(ex);

//            string h = @"          
//          [
//    {
//        'symbol': 'BTC',
//        'amount': 0.1,
//        'type': 'Purchase',
//        'createdAt': '2018-09-28T13:44:53.7907944+00:00',
//        'balance': {
//            'token': '19aaa2d2-9299-4a2b-bb3d-06ffc006e4f5',
//            'usd': 4333.264637314,
//            'btc': 0.1,
//            'eth': 0,
//            'xrp': 0
//        },
//        'exchangeRates': {
//            'btc': 6667.35362686,
//            'eth': 225.31455189,
//            'xrp': 0.52732946
//        }
//    },
//    {
//        'symbol': 'BTC',
//        'amount': 0.1,
//        'type': 'Purchase',
//        'createdAt': '2018-09-28T13:56:09.7307327+00:00',
//        'balance': {
//            'token': '19aaa2d2-9299-4a2b-bb3d-06ffc006e4f5',
//            'usd': 3668.3429065519995,
//            'btc': 0.2,
//            'eth': 0,
//            'xrp': 0
//        },
//        'exchangeRates': {
//            'btc': 6649.21730762,
//            'eth': 226.01705464,
//            'xrp': 0.53143514
//        }
//    }
//]     ";
//            IHistory hi = new History(h);
//            //List<IOneHistory> myHistory = new List<IOneHistory>();
//            //myHistory = JsonConvert.DeserializeObject<List<OneHistory>>(h);

//            string b = @"          {
//    'token': '19aaa2d2-9299-4a2b-bb3d-06ffc006e4f5',
//    'usd': 5000,
//    'btc': 0.1,
//    'eth': 45,
//    'xrp': 100
//        }";
//            Balance balance = JsonConvert.DeserializeObject<Balance>(b);
//           double dd= balance.CalculateTotalValueOfBalancesInUsd(ier);

            BuildWebHost(args).Run();
        }

        public static IWebHost BuildWebHost(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>()
                .Build();
    }
}
