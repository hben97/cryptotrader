﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using CryptoTrader2018osziFelev.Models;
using System.Net.Http;
using System.Text;
using Newtonsoft.Json;
using System.Net;
using System.IO;
using CryptoTrader2018osziFelev.Models.Interfaces;
using System.Globalization;

namespace CryptoTrader2018osziFelev.Controllers
{
    public class HomeController : Controller
    {


        [HttpPost]
        [Route("processhistory")]
        public IActionResult ProcessHistoryRequestForAjax(string inputText)
        {
            string s = "";
            for (int i = 0; i < inputText.Length && i < 200; i++)
            {
                s += inputText[i];
            }
            return Content("History működik " + s);
        }

        [HttpPost]
        [Route("processchart")]
        public IActionResult ProcessChartRequestForAjax(string inputText)
        {
            string s = "";
            for (int i = 0; i < inputText.Length && i < 200; i++)
            {
                s += inputText[i];
            }
            return Content("Charts működik" + s);
        }




        [HttpPost]
        [Route("processbalances")]
        public IActionResult ProcessBalanceRequestForAjax(string exchanges, string balances)
        {
            //exchanges $ jellel van elválasztva, abból kell listát csinálni
            //kell IExchangeRateet létrehozni, belerakni az IBalanceWithExchangeRateClasscuccba a listába
            //kell egy IBalance
            //azt is bele rakni a fenti classba
            //és már át is lehet adni a view-nak, mehet a megjelenítés :)
            return Content("Balances működik " + exchanges.Length + " - " + balances);
        }








        public IActionResult Index()
        {

            //var responseString = Sajt();

            // return Content(responseString.Result);
            //ViewData["Message"] = responseString.Result;
            //return View();
            return RedirectToAction("CryptoTrader");
        }

        //Itt most kezdődik a feldolgozás, stb..
        // private static readonly HttpClient clientHttp = new HttpClient();
        private static readonly string token = "19AAA2D2-9299-4A2B-BB3D-06FFC006E4F5";
        private static readonly string balanceLinks = "https://obudai-api.azurewebsites.net/api/exchange/btc"
            + " https://obudai-api.azurewebsites.net/api/exchange/xrp"
            + " https://obudai-api.azurewebsites.net/api/exchange/eth";

        [Route("CryptoTrader")]
        public IActionResult CryptoTrader()
        {
            //return Content(FeldolgozGetKeres("https://obudai-api.azurewebsites.net/api/exchange/btc").Result);
            return View();
        }


        private HttpResponseMessage FeldolgozGetKeres(string link)
        {
            try
            {
                HttpClient clientHttp = new HttpClient();
                clientHttp.DefaultRequestHeaders.Add("X-Access-Token", token);
                HttpResponseMessage ki = clientHttp.GetAsync(link).Result;
                return ki;
            }
            catch (Exception)
            {
                return new HttpResponseMessage(HttpStatusCode.InternalServerError);
            }




            //return await clientHttp.GetStringAsync(link);

        }


        //private HttpResponseMessage PostToApi(string link, object jsonObject)
        //{
        //    using (var httpClient = new HttpClient())
        //    {
        //        var uri = link;
        //        string contents = JsonConvert.SerializeObject(jsonObject);
        //        httpClient.DefaultRequestHeaders.Add("X-Access-Token", token);
        //       // httpClient.DefaultRequestHeaders.Add("Content-Type", "application/json");

        //        var response = httpClient.PostAsync(uri, new StringContent(contents, Encoding.UTF8, "application/json"));
        //        return response.Result;

        //    }
        //}

        private HttpResponseMessage PostToApi(string link, string jsoOrXml)
        {
            using (var httpClient = new HttpClient())
            {
                var uri = link;
                string contents = jsoOrXml;
                httpClient.DefaultRequestHeaders.Add("X-Access-Token", token);
                // httpClient.DefaultRequestHeaders.Add("Content-Type", "application/json");

                var response = httpClient.PostAsync(uri, new StringContent(contents, Encoding.UTF8, "application/json"));
                return response.Result;

            }
        }









        [Route("history")]
        public IActionResult History()
        {

            HttpResponseMessage valasz = FeldolgozGetKeres("https://obudai-api.azurewebsites.net/api/account/history");
            int i = 0;
            while (valasz.StatusCode != System.Net.HttpStatusCode.OK && i <= 100)
            {
                valasz = FeldolgozGetKeres("https://obudai-api.azurewebsites.net/api/account/history");
                i++;
            }

            if (i >= 100)
            {
                return StatusCode(500);
            }
            else
            {
                string kimenet = valasz.Content.ReadAsStringAsync().Result;

                // IHistory history = new History();
                IHistory history;
                if (kimenet.Length > 5)
                {
                
                 history = new History(kimenet);
            }           
                else
                {
                    history = new History();
                    history.Histories = new List<IOneHistory>();
                }
                
                
                return View(history);

            }
        }

        [Route("chart")]
        public IActionResult Chart(string link)
        {
            HttpResponseMessage valasz = FeldolgozGetKeres(link);
            int i = 0;
            while (valasz.StatusCode != System.Net.HttpStatusCode.OK && i <= 100)
            {
                valasz = FeldolgozGetKeres(link);
                i++;
            }

            if (i >= 100)
            {
                return StatusCode(500);
            }
            else
            {
                string kimenet = valasz.Content.ReadAsStringAsync().Result;

                IExchangeRate chart = new ExchangeRate(kimenet);
                
                return View(chart);
                //return Content(kimenet);
            }
        }

        [Route("balances")]
        public IActionResult Balances()
        {
            var linkek = balanceLinks.Split(" ");


            for (int i = 0; i < linkek.Length; i++)
            {

                HttpResponseMessage valasz = FeldolgozGetKeres(linkek[i]);
                int j = 0;
                while (valasz.StatusCode != System.Net.HttpStatusCode.OK && j <= 100)
                {
                    valasz = FeldolgozGetKeres(linkek[i]);
                    j++;
                }

                if (j >= 100)
                {
                    return StatusCode(500);
                }
                else
                {
                    string kimenet = valasz.Content.ReadAsStringAsync().Result;
                    linkek[i] = kimenet;
                }






            }




            HttpResponseMessage valaszom = FeldolgozGetKeres("https://obudai-api.azurewebsites.net/api/account");
            int z = 0;
            while (valaszom.StatusCode != System.Net.HttpStatusCode.OK && z <= 100)
            {
                valaszom = FeldolgozGetKeres("https://obudai-api.azurewebsites.net/api/account");
                z++;
            }

            if (z >= 100)
            {
                return StatusCode(500);
            }
            else
            {
                string kikuld = valaszom.Content.ReadAsStringAsync().Result;

                //return Content(kikuld + linkek[2]);
                IBalanceWithExchangeRate atad = new BalanceWithExchangeRates();
              
                for (int i = 0; i < linkek.Length; i++)
                {
                    IExchangeRate ex = new ExchangeRate(linkek[i]);
                    atad.ExchangeRates.Add(ex);
                }


                atad.Balance = new Balance(kikuld);
                ViewBag.valami = atad;
                return View();
                //return Content(kikuld);    
            //return StatusCode(500);
            }


        






    

        }

        [Route("buy")]
        public IActionResult Buy(string symbol, string amount)
        {
            if (symbol == "" || amount == "" || symbol == null || amount == null) return BadRequest();

            var clone = (CultureInfo)CultureInfo.CurrentCulture.Clone();
            clone.NumberFormat.NumberDecimalSeparator = ".";

            IBuyOrSell obj = new BuyOrSell(TransactionType.BUY, symbol, double.Parse(amount, clone));
          

            var a = PostToApi("https://obudai-api.azurewebsites.net/api/account/purchase", obj.JsonOutputText);
            int i = 0;
            while (a.StatusCode != HttpStatusCode.OK && i <= 100)
            {
                if (a.StatusCode == HttpStatusCode.BadRequest)
                {
                    return BadRequest();
                }
                a = PostToApi("https://obudai-api.azurewebsites.net/api/account/purchase", obj.JsonOutputText);
                i++;
            }
            if (i >= 100) return NoContent();
            if (a.StatusCode == HttpStatusCode.OK)
            {
                return Ok();
            }

            else
            {
                return BadRequest();
            }
        }

        [Route("sell")]
        public IActionResult Sell(string symbol, string amount)
        {
            if (symbol == "" || amount == "" || symbol == null || amount == null) return BadRequest();

            var clone = (CultureInfo)CultureInfo.CurrentCulture.Clone();
            clone.NumberFormat.NumberDecimalSeparator = ".";
            IBuyOrSell obj = new BuyOrSell(TransactionType.SELL, symbol, double.Parse(amount, clone));


            var a = PostToApi("https://obudai-api.azurewebsites.net/api/account/sell", obj.JsonOutputText);

            int i = 0;
            while (a.StatusCode != HttpStatusCode.OK && i<= 100)
            {
                if (a.StatusCode == HttpStatusCode.BadRequest)
                {
                    return BadRequest();
                }
                a = PostToApi("https://obudai-api.azurewebsites.net/api/account/sell", obj.JsonOutputText);
                i++;
            }
            if (i >= 100) return NoContent();
            if (a.StatusCode == HttpStatusCode.OK)
            {
                return Ok();
            }

            else
            {
                return BadRequest();
            }
        }

        //Vége a feldolgozásnak...

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
