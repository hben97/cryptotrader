﻿var token = "19AAA2D2-9299-4A2B-BB3D-06FFC006E4F5";
var balanceLinks = "https://obudai-api.azurewebsites.net/api/exchange/btc"
    + " https://obudai-api.azurewebsites.net/api/exchange/xrp"
    + " https://obudai-api.azurewebsites.net/api/exchange/eth";

var historyLoaded = false;
var balancesLoaded = false;
var chartLoaded = false;

var balanceNeedToBeUpdated = false;

function getHistoryAjax() {
   
    var data = null;
    var xhr = new XMLHttpRequest();
    xhr.open("GET", "https://obudai-api.azurewebsites.net/api/account/history", false);
    xhr.setRequestHeader("X-Access-Token", token);

    xhr.addEventListener("readystatechange", function () {
        if (this.readyState === 4) {
            if (this.status === 200) {
                processHistoryAjax(this.responseText);
                           
            }
            else {
                //MegjelenitFeljovoablak("Hiba: " + this.status + " - (Külső szerver hiba) - History");
                
                console.log("Hiba: " + this.status + " - (Külső szerver hiba) - History" + new Date().getHours() + ":" + new Date().getMinutes() + ":" + new Date().getSeconds());
                setTimeout(function () { getHistoryAjax(); }, 200);

            }
        }
    });

    xhr.send(data);
    
}

function processHistoryAjax(inputTextToProcess) {
    var historydiv = document.getElementById("cryptohistory");
    var data = new FormData();
    data.append("inputText", inputTextToProcess);
    var xhr = new XMLHttpRequest();
    xhr.open("POST", "./processhistory", false);
    //xhr.setRequestHeader("inputText", inputTextToProcess);

    xhr.addEventListener("readystatechange", function () {
        if (this.readyState === 4) {
            if (this.status === 200) {
                historydiv.innerHTML = this.responseText;
                historyLoaded = true;
            }
            else {
                //MegjelenitFeljovoablak("Hiba: " + this.status + " - (Belső szerver hiba) - History");
               
                console.log("Hiba: " + this.status + " - (Belső szerver hiba) - History" + new Date().getHours() + ":" + new Date().getMinutes() + ":" + new Date().getSeconds());
                setTimeout(function () { processHistoryAjax(inputTextToProcess); }, 200);
            }
        }
    });

    xhr.send(data);
}



function getChartAjax(link) {

    var data = null;
    var xhr = new XMLHttpRequest();
    xhr.open("GET", link, false);
    xhr.setRequestHeader("X-Access-Token", token);

    xhr.addEventListener("readystatechange", function () {
        if (this.readyState === 4) {
            if (this.status === 200) {
             
                    processChartAjax(this.responseText);
             
               
            }
            else {
               // MegjelenitFeljovoablak("Hiba: " + this.status + " - (Külső szerver hiba) - Charts");

                
                console.log("Hiba: " + this.status + " - (Külső szerver hiba) - Charts" + new Date().getHours() + ":" + new Date().getMinutes() + ":" + new Date().getSeconds());
                setTimeout(function () { getChartAjax(link); }, 200);
            }
        }
    });

    xhr.send(data);
    
}

function processChartAjax(inputTextToProcess) {
    var chartDiv = document.getElementById("cryptochart");
    var data = new FormData();
    data.append("inputText", inputTextToProcess);
    var xhr = new XMLHttpRequest();
    xhr.open("POST", "./processchart", false);

    xhr.addEventListener("readystatechange", function () {
        if (this.readyState === 4) {
            if (this.status === 200) {
                chartDiv.innerHTML = this.responseText;
                chartLoaded = true;
            }
            else {
                //MegjelenitFeljovoablak("Hiba: " + this.status + " - (Belső szerver hiba) - Charts");
                
                console.log("Hiba: " + this.status + " - (Belső szerver hiba) - Charts" + new Date().getHours() + ":" + new Date().getMinutes() + ":" + new Date().getSeconds());
                setTimeout(function () { processChartAjax(inputTextToProcess); }, 200);

            }
        }
    });

    xhr.send(data);
}




function getBalancesAjax(linkForBalancesDividedWithSpaces) {
   
    var linkek = linkForBalancesDividedWithSpaces.split(" ");

    //csinálni egy string változót
    //egyesével lekérni minden lista adatát
    //hozzáfűzni a stinghez $ -jel elválasztva
    //lekérni a balancesokat
    //majd 2 paramétert átadni a belső szervernek és legyártani a balancesokat :) 

    var kimenet = "";
    var hanyDarabVanBenne = linkek.length;

    var kimenetObject = { kimenet: "" };

    
    //exchangerates:
    for (var i = 0; i < linkek.length; i++) {
        processArrayElementsForBalances(linkek[i], kimenetObject);
    }

    kimenet = kimenetObject.kimenet;

    if (kimenet !== "") {
        beforeProcessBalancesAjax(kimenet);
    }
   

}

function processArrayElementsForBalances(element, kimenetObject) {
    var data = null;
    var xhr = new XMLHttpRequest();

    xhr.open("GET", element, false);
    xhr.setRequestHeader("X-Access-Token", token);

    xhr.addEventListener("readystatechange", function () {
        if (this.readyState === 4) {
            if (this.status === 200) {
                if (kimenetObject.kimenet === "") {
                    kimenetObject.kimenet = this.responseText;
                }
                else {
                    kimenetObject.kimenet += "$" + this.responseText;
                }
            }
            else {
                //MegjelenitFeljovoablak("Hiba: " + this.status + " - (Külső szerver hiba) - ExchangeRates - " + element);

                console.log("Hiba: " + this.status + " - (Külső szerver hiba) - ExchangeRates" + new Date().getHours() + ":" + new Date().getMinutes() + ":" + new Date().getSeconds());
                setTimeout(function () { processArrayElementsForBalances(element, kimenetObject); }, 150);
            }
        }
    });

    xhr.send(data);
}

function beforeProcessBalancesAjax(kimenet) {
    var balances = "";
    //balances:
    var data = null;
    var xhr = new XMLHttpRequest();

    xhr.open("GET", "https://obudai-api.azurewebsites.net/api/account", false);
    xhr.setRequestHeader("X-Access-Token", token);

    xhr.addEventListener("readystatechange", function () {
        if (this.readyState === 4) {
            if (this.status === 200) {

                balances = this.responseText;

            }
            else {
                // MegjelenitFeljovoablak("Hiba: " + this.status + " - (Külső szerver hiba) - Balances");

                console.log("Hiba: " + this.status + " - (Külső szerver hiba) - Balances before part" + new Date().getHours() + ":" + new Date().getMinutes() + ":" + new Date().getSeconds());
                setTimeout(function () { beforeProcessBalancesAjax(kimenet); }, 200);
            }
        }
    });

    xhr.send(data);


    //jöhet a belső szervernek való átadás:

    processBalancesAjax(kimenet, balances);
}

function processBalancesAjax(exchangerates, balances) {

    
   // var chartDiv = document.getElementById("cryptochart");
    var data = new FormData();
    data.append("exchanges", exchangerates);
    data.append("balances", balances);
    var xhr = new XMLHttpRequest();
    xhr.open("POST", "./processbalances", false);

    xhr.addEventListener("readystatechange", function () {
        if (this.readyState === 4) {
            if (this.status === 200) {
                document.getElementById("cryptobalance").innerHTML = this.responseText;
                balancesLoaded = true;
                balanceNeedToBeUpdated = false;
                if (programmedBar === true) {
                    StopLoadingBar();
                    MegjelenitFeljovoablak("Valutáid sikeresen frissítve lettek! :)");
                }
            }
            else {
                // MegjelenitFeljovoablak("Hiba: " + this.status + " - (Belső szerver hiba) - Balances");
               
                console.log("Hiba: " + this.status + " - (Belső szerver hiba) - Balances" + new Date().getHours() + ":" + new Date().getMinutes() + ":" + new Date().getSeconds());
                setTimeout(function () { processBalancesAjax(exchangerates, balances); }, 200);
            }
        }
    });

    xhr.send(data);
}



//ha XML-t kell visszaadni, akkor módosítani kell!!!!!
function PurchaseAjax(symbol, amount) {


    var sym = "Symbol";
    var am = "Amount";
    var obj = {};
    obj[sym] = symbol;
    obj[am] = amount;
    var data = JSON.stringify(obj);

    var xhr = new XMLHttpRequest();


    xhr.addEventListener("readystatechange", function () {
        if (this.readyState === 4) {
            if (this.status === 200) {

                MegjelenitFeljovoablak("Sikeres vásárlás!" + this.responseText + " \n" + amount + " új " + symbol + " tulajdonosa lettél! :)    ")
                showPage();
                //getBalancesAjax(balanceLinks);
                balanceNeedToBeUpdated = true;
                StartLoadingBar();
                MegjelenitFeljovoablak("Valutáid frissítése folyamatban...")
              
            }
            else if (this.status === 400) {
                MegjelenitFeljovoablak("Sajnos nincs elegendő pénzed a vásárláshoz! :(");
                showPage();
            }
            else {
                //MegjelenitFeljovoablak("Hiba: " + this.status + " - (Külső szerver hiba) - Purchase");

                
                console.log("Hiba: " + this.status + " - (Külső szerver hiba) - Purchase" + new Date().getHours() + ":" + new Date().getMinutes() + ":" + new Date().getSeconds());
                setTimeout(function () { PurchaseAjax(symbol, amount); }, 10);
            }
        }
    });



    xhr.open("POST", "https://obudai-api.azurewebsites.net/api/account/purchase");
    xhr.setRequestHeader("X-Access-Token", token);
    xhr.setRequestHeader("Content-Type", "application/json");

    xhr.send(data);




}



//ha XML-t kell visszaadni, akkor módosítani kell!!!!!
function SellAjax(symbol, amount) {


    var sym = "Symbol";
    var am = "Amount";
    var obj = {};
    obj[sym] = symbol;
    obj[am] = amount;
    var data = JSON.stringify(obj);

    var xhr = new XMLHttpRequest();


    xhr.addEventListener("readystatechange", function () {
        if (this.readyState === 4) {
            if (this.status === 200) {
                MegjelenitFeljovoablak("Sikeres eladás!" + this.responseText + " \n" + amount + " régi " + symbol + " -t adtál el! :)    ")
                showPage();
                //getBalancesAjax(balanceLinks);
                balanceNeedToBeUpdated = true;
                StartLoadingBar();
                MegjelenitFeljovoablak("Valutáid frissítése folyamatban...")
               
            }
            else if (this.status === 400) {
                MegjelenitFeljovoablak("Sajnos ennyit nem tudsz eladni :(");
                showPage();
            }
            else {
                //MegjelenitFeljovoablak("Hiba: " + this.status + " - (Külső szerver hiba) - Sell");
                
                console.log("Hiba: " + this.status + " - (Külső szerver hiba) - Sell" + new Date().getHours() + ":" + new Date().getMinutes() + ":" + new Date().getSeconds());
                setTimeout(function () { SellAjax(symbol, amount); }, 10);
            }
        }
    });



    xhr.open("POST", "https://obudai-api.azurewebsites.net/api/account/sell");
    xhr.setRequestHeader("X-Access-Token", token);
    xhr.setRequestHeader("Content-Type", "application/json");

    xhr.send(data);



}



var ido;
function MegjelenitFeljovoablak(message) {
    
        clearTimeout(ido);
    
    // alert(message);
    //document.getElementById("info").innerHTML = "<div class='alertmegjelen'><span class='closebtnmegjelen' onclick = 'this.parentElement.style.display=`none`;'>&times;</span>" + message + "</div > " + document.getElementById("info").innerHTML;
    //document.getElementById("info").innerHTML = "<div style='opacity: 1;' class='alert alert-dark alert-dismissible alma'><button type = 'button' class='close' data-dismiss='alert' >&times;</button >" + message + "</div >" + document.getElementById("info").innerHTML;
    document.getElementById("info").innerHTML = "<h1><span class='badge badge-secondary'>" + message + "</span></h1>" + document.getElementById("info").innerHTML;
    
    ido = setTimeout(function () {
        document.getElementById("info").innerHTML = "";

    }, 5000);

}


    

var fr = setInterval(function () {


    if (balanceNeedToBeUpdated === true || balancesLoaded === false) {
       
        getBalancesAjax(balanceLinks);
       
    }

    if (historyLoaded === false) {
        getHistoryAjax();
    }
    if (chartLoaded === false) {
        getChartAjax("https://obudai-api.azurewebsites.net/api/exchange/btc");
    }



    
}, 1500);

var programmedBar = false;
function StartLoadingBar() {
    programmedBar = true;
    document.getElementById("loader").style.display = "inherit";
}
function StopLoadingBar() {
    document.getElementById("loader").style.display = "none";
    programmedBar = false;
}

//egyszer mindent be kell tölteni
setTimeout(function () {
    getChartAjax("https://obudai-api.azurewebsites.net/api/exchange/btc");
    getBalancesAjax(balanceLinks);
    getHistoryAjax();
}, 1);





function showPage() {
    document.getElementById("loader").style.display = "none";
    document.getElementById("myDiv").style.display = "block";
}
var time = 0;
function Betoltes() {
   // chartLoaded = false;
    //historyLoaded = false;
    //balancesLoaded = false;
    
   
    var betolt = setInterval(function () {
        if (chartLoaded && balancesLoaded && historyLoaded) {
            showPage();
            clearInterval(betolt);
        }
        else {
            if (time % 60 === 0 && time !== 0) {
               // showPage();
               // MegjelenitFeljovoablak("Időtúllépés - A szerver jelenleg nem elérhető :(");
                document.getElementById("info").innerHTML = "<div class='alert alert-dark alert-dismissible alma'><button type = 'button' class='close' data-dismiss='alert' >&times;</button >" + "Időtúllépés - A szerver  jelenleg nem elérhető :( <button class='btn' type='button' onclick='location.reload();'>Újrapróbálkozás</button>" + "</div >" + document.getElementById("info").innerHTML;
               // document.getElementById("info").innerHTML = "<div class='alert alert-dark alert-dismissible alma'><button type = 'button' class='close' data-dismiss='alert' >&times;</button >" + "<button class='btn' type='button'>Újrapróbálkozás</button>" + "</div >" + document.getElementById("info").innerHTML;

                document.getElementById("loader").style.display = "none";
                clearInterval(betolt);
                clearInterval(fr);
            }
            time++;
        }
    }, 500);
}

Betoltes();


