﻿var historyLoaded = false;
var balancesLoaded = false;
var chartLoaded = false;
var numberOfLoadedComponents = 0;
var allNumberOfComponents = 3;

var balanceNeedToBeUpdated = false;
var historyNeedToBeUpdated = false;

function getHistoryAjax() {
   
    var historydiv = document.getElementById("cryptohistory");
    var data = null;
    var xhr = new XMLHttpRequest();
    xhr.open("GET", "./history", true);

    xhr.addEventListener("readystatechange", function () {
        if (this.readyState === 4) {
            if (this.status === 200) {
                historydiv.innerHTML = this.responseText;
                historyNeedToBeUpdated = false;
                if (historyLoaded === false) {
                    numberOfLoadedComponents++;
                    setTimeout(function () { MegjelenitFeljovoablakTorolKorabbi(numberOfLoadedComponents + "/" + allNumberOfComponents + " komponens betöltve"); },1);
                   
                }
                historyLoaded = true;
                
                
            }
            else {
                //MegjelenitFeljovoablak("Hiba: " + this.status + " - (Belső szerver hiba) - History");

                console.log("Hiba: " + this.status + " - (Belső szerver hiba) - History" + new Date().getHours() + ":" + new Date().getMinutes() + ":" + new Date().getSeconds());
                setTimeout(function () { getHistoryAjax(); }, 200);
            }
        }
    });

    xhr.send(data);
}





function getChartAjax(link) {
    var chartDiv = document.getElementById("cryptochart");
    var data = new FormData();
    data.append("link", link);
    var xhr = new XMLHttpRequest();
    xhr.open("POST", "./chart", true);

    xhr.addEventListener("readystatechange", function () {
        if (this.readyState === 4) {
            if (this.status === 200) {
                chartDiv.innerHTML = this.responseText;
                if (chartLoaded === false) {
                    numberOfLoadedComponents++;
                    setTimeout(function () { MegjelenitFeljovoablakTorolKorabbi(numberOfLoadedComponents + "/" + allNumberOfComponents + " komponens betöltve"); }, 1);
                }
                chartLoaded = true;
               
            }
            else {
                //MegjelenitFeljovoablak("Hiba: " + this.status + " - (Belső szerver hiba) - Charts");
                
                console.log("Hiba: " + this.status + " - (Belső szerver hiba) - Charts" + new Date().getHours() + ":" + new Date().getMinutes() + ":" + new Date().getSeconds());
                setTimeout(function () { getChartAjax(link); }, 200);

            }
        }
    });

    xhr.send(data);
}


function getBalancesAjax() {
 
    var data = null;
   
    var xhr = new XMLHttpRequest();
    xhr.open("GET", "./balances", true);

    xhr.addEventListener("readystatechange", function () {
        if (this.readyState === 4) {
            if (this.status === 200) {
                document.getElementById("cryptobalance").innerHTML = this.responseText;
               
                if (balancesLoaded === false) {
                    numberOfLoadedComponents++;
                    setTimeout(function () { MegjelenitFeljovoablakTorolKorabbi(numberOfLoadedComponents + "/" + allNumberOfComponents + " komponens betöltve"); }, 1);
                }
                balancesLoaded = true;
                balanceNeedToBeUpdated = false;
                if (programmedBar === true) {
                    StopLoadingBar();
                    MegjelenitFeljovoablak("Valutáid sikeresen frissítve lettek! :)");
                }
            }
            else {
                //MegjelenitFeljovoablak("Hiba: " + this.status + " - (Belső szerver hiba) - Charts");

                console.log("Hiba: " + this.status + " - (Belső szerver hiba) - Charts" + new Date().getHours() + ":" + new Date().getMinutes() + ":" + new Date().getSeconds());
                setTimeout(function () { getBalancesAjax(); }, 200);

            }
        }
    });

    xhr.send(data);
}

















//ha XML-t kell visszaadni, akkor módosítani kell!!!!!
function PurchaseAjax(symbol, amount) {


    var data = new FormData();
    data.append("symbol", symbol);
    data.append("amount", amount);
    var xhr = new XMLHttpRequest();


    xhr.addEventListener("readystatechange", function () {
        if (this.readyState === 4) {
            if (this.status === 200) {

                MegjelenitFeljovoablak("Sikeres vásárlás!" + this.responseText + " \n" + amount + " új " + symbol + " tulajdonosa lettél! :)    ")
                showPage();
                //getBalancesAjax(balanceLinks);
                //balanceNeedToBeUpdated = true;
                //historyNeedToBeUpdated = true;
                getBalancesAjax();
                getHistoryAjax();
                StartLoadingBar();
                MegjelenitFeljovoablak("Valutáid frissítése folyamatban...")
              
            }
            else if (this.status === 400) {
                MegjelenitFeljovoablak("Sajnos nincs elegendő pénzed a vásárláshoz! :(");
                showPage();
            }
            else {
                //MegjelenitFeljovoablak("Hiba: " + this.status + " - (Külső szerver hiba) - Purchase");

                
                console.log("Hiba: " + this.status + " - (Külső szerver hiba) - Purchase" + new Date().getHours() + ":" + new Date().getMinutes() + ":" + new Date().getSeconds());
                setTimeout(function () { PurchaseAjax(symbol, amount); }, 10);
            }
        }
    });



    xhr.open("POST", "./buy");
    //xhr.setRequestHeader("X-Access-Token", token);
    //xhr.setRequestHeader("Content-Type", "application/json");

    xhr.send(data);




}



//ha XML-t kell visszaadni, akkor módosítani kell!!!!!
function SellAjax(symbol, amount) {


    var data = new FormData();
    data.append("symbol", symbol);
    data.append("amount", amount);
    var xhr = new XMLHttpRequest();


    xhr.addEventListener("readystatechange", function () {
        if (this.readyState === 4) {
            if (this.status === 200) {
                MegjelenitFeljovoablak("Sikeres eladás!" + this.responseText + " \n" + amount + " régi " + symbol + " -t adtál el! :)    ")
                showPage();
                //getBalancesAjax(balanceLinks);
                //balanceNeedToBeUpdated = true;
                //historyNeedToBeUpdated = true;
                getBalancesAjax();
                getHistoryAjax();
                StartLoadingBar();
                MegjelenitFeljovoablak("Valutáid frissítése folyamatban...")
               
            }
            else if (this.status === 400) {
                MegjelenitFeljovoablak("Sajnos ennyit nem tudsz eladni :(");
                showPage();
            }
            else {
                //MegjelenitFeljovoablak("Hiba: " + this.status + " - (Külső szerver hiba) - Sell");
                
                console.log("Hiba: " + this.status + " - (Külső szerver hiba) - Sell" + new Date().getHours() + ":" + new Date().getMinutes() + ":" + new Date().getSeconds());
                setTimeout(function () { SellAjax(symbol, amount); }, 10);
            }
        }
    });



    xhr.open("POST", "./sell");

    xhr.send(data);



}



var ido;
function MegjelenitFeljovoablak(message) {
    
        clearTimeout(ido);
    
    // alert(message);
    //document.getElementById("info").innerHTML = "<div class='alertmegjelen'><span class='closebtnmegjelen' onclick = 'this.parentElement.style.display=`none`;'>&times;</span>" + message + "</div > " + document.getElementById("info").innerHTML;
    //document.getElementById("info").innerHTML = "<div style='opacity: 1;' class='alert alert-dark alert-dismissible alma'><button type = 'button' class='close' data-dismiss='alert' >&times;</button >" + message + "</div >" + document.getElementById("info").innerHTML;
    document.getElementById("info").innerHTML = "<h1><span class='badge badge-secondary'>" + message + "</span></h1>" + document.getElementById("info").innerHTML;
    
    ido = setTimeout(function () {
        document.getElementById("info").innerHTML = "";

    }, 5000);

}

function MegjelenitFeljovoablakTorolKorabbi(message) {

    clearTimeout(ido);

    // alert(message);
    //document.getElementById("info").innerHTML = "<div class='alertmegjelen'><span class='closebtnmegjelen' onclick = 'this.parentElement.style.display=`none`;'>&times;</span>" + message + "</div > " + document.getElementById("info").innerHTML;
    //document.getElementById("info").innerHTML = "<div style='opacity: 1;' class='alert alert-dark alert-dismissible alma'><button type = 'button' class='close' data-dismiss='alert' >&times;</button >" + message + "</div >" + document.getElementById("info").innerHTML;
    document.getElementById("info").innerHTML = "<h1><span class='badge badge-secondary'>" + message + "</span></h1>";

    ido = setTimeout(function () {
        document.getElementById("info").innerHTML = "";

    }, 5000);

}



//var fr = setInterval(function () {


//    if (balanceNeedToBeUpdated === true || balancesLoaded === false) {
       
//        getBalancesAjax();
       
//    }

//    if (historyLoaded === false || historyNeedToBeUpdated) {
//        getHistoryAjax();
//    }
//    if (chartLoaded === false) {
//        getChartAjax("https://obudai-api.azurewebsites.net/api/exchange/btc");
//    }



    
//}, 1500);

var programmedBar = false;
function StartLoadingBar() {
    programmedBar = true;
    document.getElementById("loader").style.display = "inherit";
}
function StopLoadingBar() {
    document.getElementById("loader").style.display = "none";
    programmedBar = false;
}

//egyszer mindent be kell tölteni
setTimeout(function () {
    getChartAjax("https://obudai-api.azurewebsites.net/api/exchange/btc");
    getBalancesAjax();
    getHistoryAjax();
}, 1);





function showPage() {
    document.getElementById("loader").style.display = "none";
    document.getElementById("myDiv").style.display = "block";
}
var time = 0;
function Betoltes() {
   // chartLoaded = false;
    //historyLoaded = false;
    //balancesLoaded = false;
    
   
    var betolt = setInterval(function () {
        if (chartLoaded && balancesLoaded && historyLoaded) {
            showPage();
            clearInterval(betolt);
        }
        else {
            if (time % 60 === 0 && time !== 0) {
               // showPage();
               // MegjelenitFeljovoablak("Időtúllépés - A szerver jelenleg nem elérhető :(");
                document.getElementById("info").innerHTML = "<div class='alert alert-dark alert-dismissible alma'><button type = 'button' class='close' data-dismiss='alert' >&times;</button >" + "Időtúllépés - A szerver  jelenleg nem elérhető :( <button class='btn' type='button' onclick='location.reload();'>Újrapróbálkozás</button>" + "</div >" + document.getElementById("info").innerHTML;
               // document.getElementById("info").innerHTML = "<div class='alert alert-dark alert-dismissible alma'><button type = 'button' class='close' data-dismiss='alert' >&times;</button >" + "<button class='btn' type='button'>Újrapróbálkozás</button>" + "</div >" + document.getElementById("info").innerHTML;

                document.getElementById("loader").style.display = "none";
                clearInterval(betolt);
                clearInterval(fr);
            }
            time++;
        }
    }, 500);
}

Betoltes();


